/**
 * Created by: Vladyslav
 * Date: 19/01/2019
 * Time: 11:42
 */
import React, { Fragment } from 'react';
import { withStyles } from '@material-ui/core/styles';
import PropTypes from 'prop-types';
import styles from './AppStyles';
import { APP_TITLE } from './configs';
import { Footer, Nav } from './components';
import { Main } from './layouts';
import './styles/index.scss';

const App = ({ classes }) => {
    return (
        <Fragment>
            { /* Navigation */ }
            <Nav title={ APP_TITLE }/>
            { /* Main container */ }
            <div className={ classes.App }>
                <Main/>
            </div>
            { /* Footer */ }
            <Footer title={ APP_TITLE }/>
        </Fragment>
    );
};

App.propTypes = {
    classes: PropTypes.object.isRequired
};

export default withStyles(styles)(App);

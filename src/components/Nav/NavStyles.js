/*
 * Created by: Vladyslav
 * Date: 19/01/2019
 * Time: 11:32
 */
export default () => ({
    root: {
        backgroundColor: '#000',
        flexGrow: 1
    },
    grow: {
        flexGrow: 1
    }
});

/**
 * Created by: Jax
 * Date: 2019-01-19
 * Time: 15:47
 */

import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import { AppBar, Toolbar, Typography } from '@material-ui/core';
import styles from './NavStyles';

const Nav = props => {
    const { classes, title } = props;

    return (
        <div className={ classes.root }>
            <AppBar position="static"
                    style={ { backgroundColor: '#000' } }>
                <Toolbar>
                    <Typography className={ classes.grow }
                                variant="h6"
                                color="inherit">
                        { title }
                    </Typography>
                </Toolbar>
            </AppBar>
        </div>
    );
};

Nav.propTypes = {
    classes: PropTypes.object.isRequired,
    title: PropTypes.string.isRequired
};

export default withStyles(styles)(Nav);

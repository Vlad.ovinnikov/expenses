/**
 * Created by: Jax
 * Date: 15/10/2018
 * Time: 18:32
 */
const styles = () => ({
    formControl: {
        marginTop: '15px',
        padding: '10px'
    },
    label: {
        paddingLeft: '10px'
    }
});
export default styles;

/**
 * Created by: Jax
 * Date: 2019-01-19
 * Time: 11:47
 */
import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import styles from './TableExpensesStyles.scss';
import './TableExpensesStyles.scss';
import { Button } from '@material-ui/core';

const TableExpenses = props => {
    const { items, rate } = props,
        // Fix floats digits, but two
        fixFloat = amount => {
            return Math.round(amount * 100) / 100;
            // return (amount / rate).toFixed(2);
        },
        // Remove item by index
        removeItem = idx => {
            return () => {
                props.removeItem(idx);
            };
        },
        // Do rendering tables header
        renderTableHeader = () => {
            return <thead className="thead-dark">
            <tr>
                <th scope="col">Title</th>
                <th scope="col">Amount (PLN)</th>
                <th scope="col">Amount (EUR)</th>
                <th scope="col">Options</th>
            </tr>
            </thead>;
        },
        // Do rendering tables body
        renderTableBody = tableItems => {
            return <tbody>
            { tableItems.map((item, key) => {
                const { title, amount } = item,
                    // Add stripe class for even items
                    isStriped = k => {
                        return k % 2 === 1 ? styles.striped : '';
                    };

                // For more complicated tables, should create Tr component
                return <tr key={ key }
                           className={ isStriped(key) }>
                    <td>{ title }</td>
                    <td>{ amount }</td>
                    <td>{ parseFloat(fixFloat(amount / rate)) }</td>
                    <td>
                        <Button fullWidth
                                onClick={ removeItem(key) }
                                type="button">
                            Delete
                        </Button>
                    </td>
                </tr>;
            }) }
            </tbody>;
        },
        // Count PLN's and EUR's amounts
        getSum = tableItems => {
            if (!tableItems) {
                return '';
            }

            const count = tableItems.reduce((accumulator, currentValue) => {
                accumulator.amountPln += parseFloat(currentValue.amount);
                accumulator.amountEur += parseFloat(fixFloat(currentValue.amount / rate));

                return accumulator;
            }, { amountPln: 0, amountEur: 0 });

            return <div>
                { `Sum: ${ count.amountPln } PLN (${ fixFloat(count.amountEur) } EUR)` }
            </div>;
        };

    return (
        <div className={ styles.container }>
            <table className={ styles.table }>
                { renderTableHeader() }
                { renderTableBody(items) }
            </table>
            { getSum(items) }
        </div>
    );
};

TableExpenses.propTypes = {
    classes: PropTypes.object.isRequired,
    rate: PropTypes.number.isRequired,
    items: PropTypes.array.isRequired
};

export default withStyles({})(TableExpenses);
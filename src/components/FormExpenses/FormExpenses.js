/**
 * Created by: Jax
 * Date: 2019-01-19
 * Time: 11:48
 */
import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import { Field, reduxForm } from 'redux-form';
import { Button, TextField } from '@material-ui/core';
import { withStyles } from '@material-ui/core/styles';
import { InputField } from '../../components';
import styles from './FormExpensesStyles.scss';
import './FormExpensesStyles.scss';

class FormExpenses extends PureComponent {

    static propTypes = {
        classes: PropTypes.object.isRequired,
        addItem: PropTypes.func.isRequired,
        rate: PropTypes.number.isRequired,
        getRate: PropTypes.func.isRequired
    };

    state = {
        title: '',
        amount: '',
        rate: 0
    };

    constructor(props) {
        super(props);

        this.onSubmit = this.onSubmit.bind(this);
        this.handleChange = this.handleChange.bind(this);
    }

    componentDidMount() {
        // Set state for from prop rate
        this.setState({ rate: this.props.rate }, () => {
            this.props.initialize(this.state);
        });
    }

    // Send values from form to Main.js
    onSubmit(values) {
        const { addItem, reset } = this.props;
        addItem(values);
        reset(); // reset form
    }

    // Handle input changes
    handleChange(name) {
        return event => {
            this.setState({ [name]: parseFloat(event.target.value) }, () => {
                // Send rate changes to the Main.js
                this.props.getRate(this.state.rate);
            });
        };
    }

    render() {
        const { handleSubmit, pristine, submitting, classes } = this.props,
            { rate } = this.state;

        return (
            <div>
                <form onSubmit={ handleSubmit(this.onSubmit) }>
                    <div className={ styles.GridContainerFullWidth }>
                        <div className={ styles.GridContainer }>
                            <Field className={ classes.formControl }
                                   name="title"
                                   type="text"
                                   component={ InputField }
                                   label="Title of transaction"/>
                        </div>
                        <div className={ classNames(styles.fixRightPadding, styles.GridContainer) }>
                            <TextField id="rate"
                                       label="Conversion rate to EUR"
                                       className={ classes.formControl }
                                       value={ rate }
                                       onChange={ this.handleChange('rate') }
                                       margin="normal"
                                       fullWidth/>
                        </div>
                    </div>
                    <div className={ styles.GridContainerFullWidth }>
                        <div className={ styles.GridContainer }>
                            <Field className={ classes.formControl }
                                   name="amount"
                                   type="text"
                                   component={ InputField }
                                   label="Amount (in PLN)"/>
                        </div>
                        <div className={ classNames(styles.fixRightPadding, styles.GridContainer) }>
                            <Button variant="outlined"
                                // color="secondary"
                                    className={ styles.button }
                                    disabled={ pristine || submitting }
                                    fullWidth
                                    type="submit">
                                Add
                            </Button>
                        </div>
                    </div>
                </form>
            </div>
        );
    }
}

const validate = values => {
    const { title, amount } = values,
        errors = {},
        requiredFields = ['title', 'amount'],
        pttrDecimal = /^(?=.?\d)\d*([,.]\d{0,2})?$/,
        pttrDigits = /^[0-9]+([,.][0-9]+)?$/g;

    requiredFields.forEach(field => {
        if (!values[field]) {
            errors[field] = 'Required';
        }
    });

    if (title && title.length < 5) {
        errors.title = 'Title should have at least 5 characters';
    }

    if (amount && !amount.match(pttrDigits)) {
        errors.amount = 'Should be number or decimal';

    } else if (amount && !amount.match(pttrDecimal)) {
        errors.amount = 'Should be 2 digits after the decimal point';
    }

    return errors;
};

export default reduxForm({
    form: 'FormExpenses',
    validate
})(withStyles({})(FormExpenses));
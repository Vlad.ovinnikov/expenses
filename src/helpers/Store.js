import { applyMiddleware, createStore } from 'redux';
import promise from 'redux-promise';
import reducers from '../reducers';

const store = createStore(
    reducers,
    {},
    applyMiddleware(promise)
);

export default store;
